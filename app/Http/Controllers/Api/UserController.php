<?php

namespace App\Http\Controllers\Api;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    public function index(Request $request, $id = null)
    {
        $check = User::query()->where('api_token', $request->bearerToken())->first();

        if (!empty($id)) {
            $userId = User::query()->where('id', $id)->first();

            $Id['messages'] = 'OK';
            $Id['status'] = 200;
            $Id['results'] = $userId;

            return response($Id);
        }

        if (!empty($check)) {
            $user['messages'] = 'OK';
            $user['status'] = 200;
            $user['results'] = User::all();

            return response($user);
        } else {
            $error['messages'] = 'UNAUTHORIZED';
            $error['status'] = 400;
            $error['results'] = array();

            return response($error);
        }
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'username' => 'required|unique:users',
            'password' => 'required'
        ]);

        if ($validator->fails()) {
            $res['message'] = ("ERROR");
            $res['status'] = 500;
            $res['results'] = array();

            return response($res);
        }

        $user = new User();
        $user->name = $request->name;
        $user->username = $request->username;
        $user->password = bcrypt($request->password);
        $user->api_token = 1234;
        $user->save();

        $res['message'] = ("CREATED");
        $res['status'] = 200;
        $res['results'] = $user;

        return response($res);
    }

    public function update(Request $request, $id)
    {
        $user = User::query()->find($id);

        if (!empty($user)) {
            $user->update($request->all());
            $user->save();

            $res['message'] = ("UPDATED");
            $res['status'] = 200;
            $res['results'] = $user;

            return response($res);
        } else {
            $res['message'] = ("ERROR");
            $res['status'] = 500;
            $res['results'] = array();

            return response($res);
        }
    }

    public function destroy($id)
    {
        $user = User::query()->findOrFail($id);

        if (!empty($user)) {
            $user->delete();

            $res['message'] = ("DELETED");
            $res['status'] = 200;
            $res['results'] = $user;

            return response($res);
        }  else {
            $res['message'] = ("ERROR");
            $res['status'] = 500;
            $res['results'] = array();

            return response($res);
        }
    }
}
